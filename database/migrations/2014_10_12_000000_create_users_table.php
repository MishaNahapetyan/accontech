<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username', 60)->unique();
            $table->tinyInteger('status')->comment('0 => inactive, 1 => active');
            $table->smallInteger('admin')->comment('0 => user, 1 => admin');
            $table->text('description');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();

        });
        $pass = bcrypt(123123);
        DB::statement("INSERT INTO users (first_name, last_name, username, status, admin, description, password) 
                    VALUES ('Misha', 'Nahapetyan', 'Accontech', 1, 1, '', '$pass')");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
