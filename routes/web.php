<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard/perPage/{perPage}', 'DashboardController@index')->name('dashboard');
Route::get('/dashboard/', 'DashboardController@index')->name('dashboard');
Route::get('/dashboard/update/{id}', 'DashboardController@update')->name('update');
Route::get('/dashboard/generate', 'DashboardController@generate')->name('generate');
Route::post("dashboard/status", "DashboardController@status");
Route::post("dashboard/delete", "DashboardController@delete");
Route::post("/auth/register", "AuthController@register");
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post("/login", [
    'uses' => 'Auth\LoginController@index',
    'as' => 'login'
]);


