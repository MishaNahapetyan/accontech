<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    protected $table = "users";
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name','username','status','admin','description', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
        public function is_admin(){
            if($this->admin){

                return true;
            }else{
                return false;
            }
        }

    public static function get($data)
    {
        $result = '';
        $username = $data['username'];
        $pass = $data['password'];
            if(Auth::attempt(['username'=> $username,'password' => $pass, 'status' => 1])) {
                $result = User::where("username", "=", $username)->get()->first();
            }
        return $result;
    }

}
