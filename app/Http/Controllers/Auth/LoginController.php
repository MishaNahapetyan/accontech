<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|max:255',
            'password' => 'required',


        ]);

        if ($validator->fails()) {
            return redirect('login')
                ->withErrors($validator)
                ->withInput();
        }

        $user = User::get($request->except("_token"));
       if(empty($user)){
           return redirect()->route('login')->with('alert-danger', 'incorrect login/password.');
       }
        $payload = serialize($request->session()->all());
        $ip = $_SERVER['REMOTE_ADDR'];
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $session_id = Session::getId();
        $last_activity  = time();
        DB::statement("INSERT INTO sessions (id, user_id, ip_address, user_agent, payload, last_activity) 
                    VALUES ('$session_id','$user->id', '$ip', '$user_agent', '$payload', '$last_activity' )");
        if ($user->is_admin()) {
            return redirect()->route('dashboard');
        }
        return redirect()->route('home');

    }
    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
}
