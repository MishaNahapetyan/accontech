<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($perPage = 10)
    {
        if(!Auth::user()->is_admin()){
            return redirect()->route('home');
        }
        $users = User::where('admin', '=', 0)->paginate($perPage);
   
        return view('dashboard', [
            'user' => $users,
            'perpage' => $perPage
        ]);
    }
        public function status(Request $request){
            if(!Auth::user()->is_admin()){
                return redirect()->route('home');
            }
            if($request->ajax()){
                $id = $request->post()['id'];
                $status = $request->post()['status'];
                User::where('id', $id)
                    ->update(['status' => $status == 1 ? 0 : 1]);
                $userStatus  = User::where('id', $id)->first();
                return json_encode(['status' => $userStatus->status, 'id' => $id]);
            }

        }

        public function delete(Request $request){
            if(!Auth::user()->is_admin()){
                return redirect()->route('home');
            }
            if($request->ajax()){
                $id = $request->post()['id'];
                User::where('id', $id)
                    ->delete();
                return json_encode(['success' => 'success']);
            }
        }
    
    public function update(Request $request, $id){

        if(!Auth::user()->is_admin()){
            return redirect()->route('home');
        }
       $user =  User::where('id', $id)->get()->first();
       if(empty($user)){
           return redirect()->route('dashboard');
       }
        if(!empty(Input::get())){
            request()->validate([
                'first_name' => 'required|min:3',
                'last_name' => 'required|min:3',

            ]);

            User::where('id', $id)
                ->update(['first_name' => $request->first_name,'last_name' => $request->last_name, 'description' => !empty($request->description) ?  $request->description : '']);
            return redirect()->route('dashboard')
                ->with('success','User updated successfully');

        }

        return view('update', ['user' => $user]);
    }

    public function generate(){
        if(!Auth::user()->is_admin()){
            return redirect()->route('home');
        }
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 200; $i++)
        {
       
          User::create([
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'admin' => 0,
                'status' => 1,
                'description' => '',
                'username' => $faker->unique()->userName,
                'password' => bcrypt($faker->word),
            ]);
        }
         return redirect()->route('dashboard');
    }

}
