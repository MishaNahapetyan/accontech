@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default panel-table">
                    <div class="panel-heading clearfix">
                        <span class="pull-left">Dashboard</span>
                        <div class="pull-right clearfix" style="display: inline-block">
                            <div class="dropdown" style="display: inline-block">
                                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">{{$perpage}}
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu">
                                    <li><a href="/dashboard/perPage/10">10</a></li>
                                    <li><a href="/dashboard/perPage/20">20</a></li>
                                    <li><a href="/dashboard/perPage/50">50</a></li>
                                </ul>
                            </div>
                            <a class="btn btn-info" href="/dashboard/generate">Generate Users(200)</a>
                        </div>



                    </div>
                    <div class="panel-body">

                        <table class="table table-striped table-bordered table-list">
                            <thead>
                            <tr>
                                <th><em class="fa fa-cog"></em></th>
                                <th>First name</th>
                                <th>Last name</th>
                                <th>Username</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($user as $val)
                            <tr data-key="{{$val->id}}">
                                <td align="center">
                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" />
                                    <a href="/dashboard/update/{{$val->id}}" class="btn btn-default"><em class="fa fa-pencil"></em></a>
                                    <a class="btn btn-danger delete_user"><em class="fa fa-trash"></em></a>
                                </td>

                                <td>{{$val->first_name}}</td>
                                <td>{{$val->last_name}}</td>
                                <td>{{$val->username}}</td>
                                <td> <a class="{{$val->status == 1 ? 'btn btn-danger' : 'btn btn-success'}} change_status" data-key="{{$val->status}}">{{$val->status == 1 ? 'Inactive' : 'Active'}}</a></td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                    <div class="panel-footer">
                        <div class="row">

                            <div class="col col-xs-8">
                                {{$user->links()}}


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
