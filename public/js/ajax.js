$(document).ready(function(){
   $('body').on('click', '.change_status', function(){
      var self = $(this);
      var id = $(this).closest('tr').attr('data-key');
      var status = $(this).attr('data-key');
       var tok = $('#_token').val();
      $.ajax({
          url: "/dashboard/status",
          data: {id: id, _token : tok, status:status},
          method: "POST",
          dataType: 'json',
          success: function(res){
          $(self).attr('data-key', res.status);
          if(res.status === 1){
             $(self).removeClass('btn btn-success');
             $(self).addClass('btn btn-danger');
             $(self).text('Inactive');
          }else{
              $(self).removeClass('btn btn-danger');
              $(self).addClass('btn btn-success');
              $(self).text('Active');
          }
          }
      });
   });
   $('body').on('click', '.delete_user', function(){
       var self = $(this);
       var id = $(this).closest('tr').attr('data-key');
       var tok = $('#_token').val();
       if(!confirm('Are you sure')){
          return false;
       }
       $.ajax({
           url: "/dashboard/delete",
           data: {id: id, _token : tok},
           method: "POST",
           dataType: 'json',
           success: function(res){
             if(res.success === 'success'){
                $(self).closest('tr').remove();
             }
           }
       });
   });
});